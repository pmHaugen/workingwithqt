#include <iostream>
#include <QDebug>
#include <array>
#include <vector>
#include <tgmath.h>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <QString>
#include <QTextStream>

using namespace std;

//This is where you set n//
    const int n = 5;
///////////////////////////
    vector<int> storage;

int randGenerator(int min, int max)
{
    int random;
    bool retry;

    do
    {
        random = rand() % min + max;
        retry = false;
        for (size_t it : storage)
        {
            if(storage[it] == random)
            {
                retry = true;
                break;
            }
            if(it == storage.size())
            {
                return -1;
            }
        }
    }while(retry);
    storage.push_back(random);
    return random;
}

bool canPlace(int cord, char *board)
{
    array<char, n*n> tempBoard;

    for (int i = 0; i<n*n; i++)
    {
        tempBoard[i] = *board;
        board++;
    }
    for (int i = 0; i < n; i++)//A system to translate a 1D array to 2D cordinates so the ruleset is easier to make
    {
        for (int j = 0; j < n; j++)
        {
            ///////////////////////////////////////////RULESET/////////////////////////////////////////////
            //----When the loop has found the cordinates for the Queen the program is trying to place----//
            if((i*n)+j == cord) //in+j is a formula to translate a row of n numbers into a n*n grid
            {
                int tempI = i, tempJ = j;
                while (tempI>0 && tempJ>0) //Finding top left diagonal
                {
                    tempI--;
                    tempJ--;
                }
                while(tempI < n && tempJ < n)
                {
                    if(tempBoard[tempI*n+tempJ] == 'Q') //finding all Queens from top left to bottom right if any
                    {
                        return false;
                    }
                    tempI++;
                    tempJ++;
                }
                tempI = i;
                tempJ = j;
                while(tempI > 0 && tempJ < n) //finding top right diagonal
                {
                    tempI--;
                    tempJ++;
                }
                while(tempI <= n && tempJ >= 0)
                {
                    if(tempBoard[tempI*n+tempJ] == 'Q') //finding all Queens from top right to bottom left if any
                    {
                        return false;
                    }
                    tempI++;
                    tempJ--;
                }
                tempI = 0;
                tempJ = j;
                while(tempI < n) //Checking if any queens vertically
                {
                    if(tempBoard[tempI*n+tempJ] == 'Q')
                    {
                        return false;
                    }
                    tempI++;
                }
                tempI = i;
                tempJ = 0;
                while(tempJ < n) //Checking if any queen horizontally
                {
                    if(tempBoard[tempI*n+tempJ] == 'Q')
                    {
                        return false;
                    }
                    tempJ++;
                }
                return true; //If the program got through the whole ruleset without finding any nono's it can place the queen

            }//End of ruleset
        }
    }
    return false; //should be impossible but if the program didn't find the cordinate at all it will return false
}

void placeQueens(char *a)
{
    qDebug() << "n = " << n << "   Possible Queen locations (n*n) = " << n*n;
    array<char, n*n> board;
    int queensPlaced = 0;
    int tries = n; //sets how often the program should start from scratch depending on board size
    long long loops = 0;

    char *boardPointer = a;

    for (int i = 0; i < n*n; i++)
    {
        board[i] = *boardPointer;
        boardPointer++;
    }
    srand(time(NULL));
    int cord;
    while (queensPlaced < n)
    {
        loops++;
        cord = randGenerator(n, queensPlaced*n); //rand() % n + queensPlaced*n;
        if (cord == -1)
            tries = 0;
        if((n - (10 + log10(n))) < queensPlaced && n>14) //switching to brute force when the chances of good guess gets too low
        {
            for (int i=n*queensPlaced; i<n*n; i++)
            {
                if(canPlace(i, board.data()))
                {
                    board[i] = 'Q';
                    storage.clear();
                    queensPlaced++;
                    tries = n;
                    break;
                }
                else
                    tries = 0;
            }
        }
        else if(tries > 0 && board[cord] != 'Q' && canPlace(cord, board.data()))//The compiler checks the first statement first so the rules function doesn't get run if not needed
        {
            board[cord] = 'Q';
            storage.clear();
            queensPlaced++;
            tries = n;
        }

        tries--;
        if(tries < 0) //resets the board of no tries left
        {
            storage.clear();
            tries = n*n + queensPlaced*2;
            queensPlaced = 0;
            boardPointer = a;
            for (int i = 0; i < (n*n); i++)
            {
                board[i] = *boardPointer;
                boardPointer++;
            }
        }
    }
     qDebug() << "loops: " << loops;
    for(int i = 0; i < n; i++) //Prints the completed board
    {
        for(int j = 0; j < n; j++)
        {
             qDebug() << " " << board[i*n+j];
             qDebug() << '\a';
        }
        //cout << endl;
    }
}
int main()
{
    array<char, n*n> a;
    char * arrayPointer;
    arrayPointer = a.data();

    int tempN = 0;
    QString stringtest = "string";
    qDebug() << stringtest << "< stringtest";
    QTextStream text("texttest");
    text >> stringtest;
    qDebug() << stringtest << "< texttest";

    //qDebug() << text << "texttest";
    for (int i = 0; i<n; i++)
    {
        for(int j = 0; j<n; j++)
        {
            a[i*n+j] = '+';
            tempN++;
        }
    }
    if(n*3 < n*n)
         placeQueens(arrayPointer);
    else if(n==1)
        placeQueens(arrayPointer);
    else
         qDebug() << "No solution";
    return 0;
}
